import express, { NextFunction, Request, Response } from "express";
import PostRouter from "./post/post.routes";
const app = express();

app.use("/post", PostRouter);

const port = 3001;
app.listen(port, () => console.log(`Server is listening on port ${port}`));
