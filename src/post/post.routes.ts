import { Router } from "express";
const router = Router();

import {
  handleDelete,
  handleGet,
  handlePatch,
  handlePost,
  handlePut,
  handleSingleGet,
} from "./post.controller";

router.get("/", handleGet);
router.get("/:slug", handleSingleGet);
router.post("/post", handlePost);
router.put("/update/:slug", handlePut);
router.patch("/update/:slug", handlePatch);
router.delete("/delete/:slug", handleDelete);

export default router;
