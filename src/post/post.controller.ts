import { Request, Response } from "express";

export function handleGet(req: Request, res: Response) {
  console.log("inside / endpoint");
  return res.status(200).json({ key: "success", data: "Get Req" });
}
export function handleSingleGet(req: Request, res: Response) {
  console.log("inside / endpoint");
  const { slug } = req.params;
  return res
    .status(200)
    .json({ key: "success", data: `Get Single Req for ${slug}` });
}
export function handlePost(req: Request, res: Response) {
  res.status(201).json({ key: "success", data: "Post Request" });
}
export function handlePut(req: Request, res: Response) {
  res.status(201).json({ key: "success", data: "Put Request" });
}
export function handlePatch(req: Request, res: Response) {
  res.status(201).json({ key: "success", data: "Patch Request" });
}

export function handleDelete(req: Request, res: Response) {
  res.status(201).json({ key: "success", data: "Delete Request" });
}
